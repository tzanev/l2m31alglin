\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{macros_M31}

\begin{document}

\hautdepage{TD2$\small\frac{1}{2}$ - compléments}


% ==================================================
\section{Matrices et applications}
% ==================================================

%-----------------------------------
\begin{exo}

On travaille dans l'espace $\R^3$ muni de la base canonique $\underline{e} = (e_1,e_2,e_3)$. On considère les applications linéaires $\R^3\rightarrow\R^3$ dont la matrice dans la base $(e_1,e_2,e_3)$ s'écrit:

\begin{multicols}{3}
    \begin{enumerate}
      \item
        $
          A =
            \begin{psmallmatrix*}[r]
              -1 & \;1 & \;2 \\[3pt]
              -2 &   2 &   4 \\[3pt]
              -1 &   1 &   2
            \end{psmallmatrix*}
        $
      \item
        $
          B =
            \begin{psmallmatrix*}[r]
               1 & \ 0 & -2 \\[3pt]
               0 &   2 &  2 \\[3pt]
              -1 &   1 &  3
            \end{psmallmatrix*}
        $

      \item
        $
          C =
            \begin{psmallmatrix}
               1 & \;0 & \;1 \\[3pt]
               1 &   2 &   0 \\[3pt]
               1 &   1 &   3
            \end{psmallmatrix}
        $
    \end{enumerate}
  \end{multicols}
  Pour chacune de ces applications :
  \begin{questions}
    \item Déterminer un système minimale d'équations et une base du noyau.
    \item Utiliser des opérations selon les colonnes pour écrire la matrice sous une forme échelonnée (selon les colonnes), interpréter cette construction comme un changement de base au départ de base $\underline{e}\mapsto\underline{e}'$.
    \item Expliciter la matrice $P = P_{\underline{e}}(e_1',e_2',e_3')$ correspondant à ce changement de base ainsi que son inverse $P^{-1} = P_{\underline{e'}}(e_1,e_2,e_3)$.
    \item Utiliser le résultat obtenu dans la question précédente pour expliciter une base de de l'image et une base du noyau.
    \item Déterminer un système minimale d'équations de l'image.
  \end{questions}

\end{exo}

%-----------------------------------
\begin{exo}

  Soit $u$ l'endomorphisme de $\mathbb{R}^3$ dont la matrice dans la base canonique $(i,j,k)$ de $\mathbb{R}^3$ est:

  $$
    M=
      \begin{psmallmatrix*}[r]
        2  & 1  & 0  \\[3pt]
        -3 & -1 & 1  \\[3pt]
        1  & 0  & -1
      \end{psmallmatrix*}.
  $$

  \begin{enumerate}
    \item  Déterminer $u(2i-3j+5k)$.
    \item  Déterminer $\Ker u$ et $\Im u$.
    \item  Calculer $M^2$ et $M^3$.
    \item  Déterminer $\Ker u^2$ et $\Im u^2$.
    \item  Calculer $(I-M)(I+M+M^2)$ et en déduire que $I-M$ est inversible. Préciser $(I-M)^{-1}$.
  \end{enumerate}

\end{exo}

\newpage
%-----------------------------------
\begin{exo}

  Déterminer le rang des matrices suivantes:

  \begin{multicols}{2}
    \begin{enumerate}
      \item
        $
          A =
            \begin{psmallmatrix}
                     1 & \ \sfrac12 & \ \sfrac13 \\[3pt]
              \sfrac12 &   \sfrac13 &   \sfrac14 \\[3pt]
              \sfrac13 &   \sfrac14 &          m
          \end{psmallmatrix}
        $
      \item
        $
          B =
            \begin{psmallmatrix}
                 1  &   1   &   1   \\[3pt]
                b+c & \;c+a & \;a+b \\[3pt]
                bc  &  ca   &  ab
            \end{psmallmatrix}
        $

      \item
        $
          C =
            \scalebox{.7}{$
              \begin{pmatrix}
                1 & a & 1 & b \\
                a & 1 & b & 1 \\
                1 & b & 1 & a \\
                b & 1 & a & 1
              \end{pmatrix}
            $}
        $
      \item
        $
          D = \big[i+j+ij\big]_{1\leq i,j\leq n}
        $
    \end{enumerate}
  \end{multicols}

\end{exo}

% ==================================================
\section{Déterminants}
% ==================================================

%-----------------------------------
\begin{exo}

  Soient $a,$ $b,$ $c$ des réels.
  Calculer les déterminants suivants.

  $$
    D_{1} =
      \scalebox{.7}{$
        \begin{vmatrix}
          \ 1 & 0 & 0 & 1\ \\
          \ 0 & 1 & 0 & 0\ \\
          \ 1 & 0 & 1 & 1\ \\
          \ 2 & 3 & 1 & 1\
        \end{vmatrix}
      $}
      \quad
    D_{2} =
      \begin{vsmallmatrix}
          a+b+c &   b   &   b   &   b   \\[3pt]
            c   & a+b+c &   b   &   b   \\[3pt]
            c   &   c   & a+b+c &   b   \\[3pt]
            c   &   c   &   c   & a+b+c
      \end{vsmallmatrix}
      \quad
    D_{3} =
      \begin{vsmallmatrix}
        1 & 0 & 3 & 0 & 0 \\[3pt]
        0 & 1 & 0 & 3 & 0 \\[3pt]
        a & 0 & a & 0 & 3 \\[3pt]
        b & a & 0 & a & 0 \\[3pt]
        0 & b & 0 & 0 & a
      \end{vsmallmatrix}
  $$
  Généraliser le calcul de $D_2$ à un déterminant $n\times n$ du même type.

\end{exo}

%-----------------------------------
\begin{exo}

  Calculer le déterminant de la matrice suivante :
  $$
    \begin{psmallmatrix}
      m &    0     & 1 & \;2m \\[3pt]
      1 &    m     & 0 & \;0  \\[3pt]
      0 & \;2m+2\; & m & \;1  \\[3pt]
      m &    0     & 0 & \;m
    \end{psmallmatrix}
  $$
  Calculer alors, suivant la valeur du paramètre $m$, le rang de cette matrice.

\end{exo}

\end{document}


