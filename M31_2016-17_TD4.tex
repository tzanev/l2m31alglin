\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{macros_M31}

\begin{document}

\hautdepage{TD4}


% ==================================================
\section{Compléments de cours}
% ==================================================


%-----------------------------------
\begin{exo}

  On rappelle que la trace d'une matrice $n\times n$, soit $A = (a_{i j})_{1\leq i,j\leq n}\in\M_{n}(\K)$,
  est définie par la formule $\Tr(A) = \sum_{i=1}^n a_{i i}$.

  \begin{enumerate}
    \item Prouver que l'on a la relation $\Tr(A B) = \Tr(B A)$ pour tout couple de matrices $A,B\in\M_{n}(\K)$. En déduire que l'on a relation $\Tr(P A P^{-1}) = \Tr(A)$ pour toute matrice $A$, où $P$ est une matrice inversible.

    \item On considère la matrice $M_e^e(\phi)$ de l'endomorphisme $\phi$ dans une base $e$ de l'espace $E$ au départ et à l'arrivée. Montrer que l'on a la relation $\Tr(M_e^e(\phi)) = \Tr(M_f^f(\phi))$ lorsque l'on considère la matrice $M_f^f(\phi)$ de l'endomorphisme $\phi$ associée à un autre choix de base $f$ de l'espace $E$.\\
    \begin{indication}
      On utilisera une formule de changement de base et le résultat de la question précédente.
    \end{indication}\\
    Conclure que la définition de la trace d'un endomorphisme $\phi: E\rightarrow E$ d'un espace vectoriel $E$ par $\Tr(\phi) := \Tr(M_e^e(\phi))$ a un sens.

    \item Retrouver le résultat de la question précédente en utilisant que le déterminant d'un endomorphisme ne dépend pas du choix de la base et de la relation
      $$
        P_{\phi}(x) = (-1)^n x^n + (-1)^{n-1} \Tr(\phi) x^{n-1} + \dots + \Det(\phi)
      $$
    qui lie le polynôme caractéristique $P_{\phi}(x) = \Det(\phi - x\Id)$ et la trace $\Tr(\phi)$.
  \end{enumerate}
\end{exo}
\vspace{-2.1mm}

% ==================================================
\section{Polynômes de matrices et d'endomorphismes --- généralités}
% ==================================================


%-----------------------------------
\begin{exo}

  \begin{enumerate}
    \item Montrer que la matrice
      $$
        W = \scalebox{.7}{\ensuremath{
          \begin{pmatrix}
            0 & 1 & 0 & 0 \\
            0 & 0 & 1 & 0 \\
            0 & 0 & 0 & 1 \\
            1 & 0 & 0 & 0
          \end{pmatrix}
        }}
      $$
    est diagonalisable sur $\C$ et expliciter des vecteurs propres de $W$ formant une base de $\C^4$. On notera cette base $(w_1,w_2,w_3,w_4)$.\\
    \begin{indication}
      L'équation $W X = \lambda X$ se résoud facilement et directement, sans passer par le polynôme caractéristique de $W$.
    \end{indication}

    \item Soit $A = A(a_0,a_1,a_2,a_3)$ la matrice
      $$
        A = \scalebox{.84}{\ensuremath{
          \begin{pmatrix}
            a_0 & a_1 & a_2 & a_3 \\
            a_3 & a_0 & a_1 & a_2 \\
            a_2 & a_3 & a_0 & a_1 \\
            a_1 & a_2 & a_3 & a_0
          \end{pmatrix}.
        }}
      $$
    Observer que $A$ s'écrit comme un polynôme en $W$ (on calculera les puissances $W^i$). Comment s'écrit l'image de $w_i$ par l'application $X\mapsto A X$? On utilisera l'observation précédente pour obtenir un résultat sans calcul.

    \item Comment s'écrit la matrice de l'application $X\mapsto A X$ dans la base $(w_1,w_2,w_3,w_4)$? Utiliser le résultat obtenu pour exprimer $\Det(A)$ sous forme factorisée.

    \item\hard
    Généraliser l'exercice pour une matrice $n\times n$ de la forme:
      $$
        A = \scalebox{.84}{\ensuremath{
          \begin{pmatrix}
             a_0   &  a_1   & \cdots & \cdots &  a_n   \\
             a_n   &  a_0   &  a_1   &        & \vdots \\
            \vdots & \ddots & \ddots & \ddots & \vdots \\
             a_2   & \cdots &  a_n   &  a_0   &  a_1   \\
             a_1   &  a_2   & \cdots &  a_n   &  a_0
          \end{pmatrix}.
        }}
      $$
  \end{enumerate}

\end{exo}


% ==================================================
\section{Polynômes d'endomorphismes et diagonalisation}
% ==================================================


%-----------------------------------
\begin{exo} (exercice de compréhension du cours)

  On considère l'application linéaire $\phi = \phi_S: \R^3\rightarrow\R^3$ associée à la matrice
    $$
      S =
        \begin{pmatrix*}[r]
          0 & 1 & 0 \\
          0 & 0 & 1 \\
          -1 & 0 & 0
        \end{pmatrix*}.
    $$

  \begin{enumerate}
    \item Prouver que $\phi$ annule le polynôme $p(x) = x^3 + 1$. Dans la suite, on considère la factorisation $p(x) = f(x) g(x)$, où on pose $f(x) = x^2 - x + 1$ et $g(x) = x + 1$.

    \item Expliciter une décomposition de Bezout $u(x) f(x) + v(x) g(x) = 1$. En déduire la relation $u(\phi) f(\phi) + v(\phi) g(\phi) = \Id$.

    \item En utilisant la relation précédente montrer que $\ker(f(\phi))$ et $\ker(g(\phi))$ sont en somme directe.

    \item Montrer que l'on a $u(\phi)\cdot f(\phi)(\vv{w})\in\ker(g(\phi))$ et $v(\phi)\cdot g(\phi)(\vv{w})\in\ker(f(\phi))$, pour tout vecteur $\vv{w}\in\R^3$, en utilisant que $f(\phi) g(\phi) = p(\phi) = 0$. Puis conclure que l'on a la relation $\ker(f(\phi))\oplus\ker(g(\phi)) = \R^3$.

    \item Prouver que l'endomorphisme $r(\phi) = u(\phi)\cdot f(\phi)$ définit en fait un projecteur de $\R^3$ sur l'espace $\ker(g(\phi))$ parallèlement au sous espace $\ker(f(\phi))$, et que l'endomorphisme $s(\phi) = v(\phi)\cdot g(\phi) = \Id - r(\phi)$ définit, de façon symétrique, un projecteur de $\R^3$ sur l'espace $\ker(g(\phi))$ parallèlement au sous espace $\ker(f(\phi))$. Expliciter ces projecteurs $r(\phi)$ et $s(\phi)$ pour l'endomorphisme $\phi = \phi_S$ considéré dans l'exercice.

  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo}

  Soit $X$ une matrice $n\times n$ à coefficients réels telle que $I + X + X^2 = 0$.

  \begin{enumerate}
    \item Montrer que $X$ est diagonalisable (sur $\C$) et donner ses valeurs propres.

    \item Que peut-on dire de la dimension $n$, du déterminant et de la trace de $X$?

  \end{enumerate}

\end{exo}

%-----------------------------------
\begin{exo}

  Soient $\phi,\psi: E\rightarrow E$ des endomorphismes d'un espace vectoriel $E$ de dimension finie $n$. On suppose que $\phi$ et $\psi$ sont diagonalisables et commutent ($\phi\circ\psi=\psi\circ\phi$). On note $\lambda_i$, $i = 1,\ldots,r$ les valeurs propres de $\phi$ et $E_{\lambda_i} = \{v\in E|\phi(v) = \lambda_i v\}$, $i = 1,\ldots,r$, les espaces propres correspondants.

  \begin{enumerate}
    \item Prouver que l'on a $\psi(E_{\lambda_i})\subset E_{\lambda_i}$ pour tout $i = 1,\dots,r$. On considère alors l'application $\psi_i: E_{\lambda_i}\rightarrow E_{\lambda_i}$ obtenue par restriction de $\psi: E\rightarrow E$ au sous espace $E_{\lambda_i}\subset E$.

    \item Observer que $\psi_i$ annule un polynôme scindé à racines simples. Conclure que chaque endomorphisme $\psi_i: E_{\lambda_i}\rightarrow E_{\lambda_i}$ est diagonalisable.

    \item Déduire du résultat précédent que $\phi$ et $\psi$ diagonalisent dans une base commune de vecteurs propres.

    \item\hard Expliciter une base commune de vecteurs propres pour les endomorphismes $\phi = \phi_A$ et $\psi = \psi_B$ associés aux matrices
      % $$
      %   A =
      %     \begin{pmatrix*}[r]
      %       1 & 2 & -1 & -1 \\
      %       2 & 1 & -1 & -1 \\
      %       1 & 1 & 0  & -2 \\
      %       1 & 1 & -2 & 0
      %     \end{pmatrix*}
      %   \quad \text{et} \quad
      %   B =
      %     \begin{pmatrix}
      %       0 & -1 & -1 & 2 \\
      %       1 & -1 & -2 & 1 \\
      %       1 & -2 & -1 & 1 \\
      %       2 & -1 & -1 & 0
      %     \end{pmatrix},
      % $$
      $$
        A =
          \begin{pmatrix*}[r]
            1 & \phantom{-}1 & -1 \\
            0 &            2 &  0 \\
            0 &            0 &  2
          \end{pmatrix*}
        \quad \text{et} \quad
        B =
          \begin{pmatrix*}[r]
            -1 & \phantom{-}2 & -1 \\
             0 &            3 & -4 \\
             0 &            2 & -3
          \end{pmatrix*},
      $$
    après avoir vérifié que ces matrices satisfont bien à la relation $A B = B A$.
  \end{enumerate}
\end{exo}


% ==================================================
\section{Sous espaces caractéristiques et décomposition de Dunford\footnotemark[1]}
% ==================================================
\footnotetext[1]{Qui devrait s'appeler «de Jordan--Chevalley», car les travaux de Dunford sont postérieurs à ceux de Jordan et Chevalley.}

%-----------------------------------
\begin{exo}

  On considère l'application linéraire $\phi: \R^4\rightarrow\R^4$ associée à la matrice
  $$
    T =
      \begin{pmatrix}
        0 & 1 & 1 & 0 \\
        0 & 0 & 0 & 1 \\
        1 & 0 & 0 & 1 \\
        0 & 1 & 0 & 0
      \end{pmatrix}.
  $$

  \begin{enumerate}
    \item Prouver que l'on a la relation $(\phi-\Id)^2\cdot(\phi+\Id)^2 = 0$, soit en calculant directement cet endomorphisme composé, soit en appliquant le théorème de Cayley-Hamilton.

    \item Prouver que les espaces $F = \ker(\phi-\Id)^2$ et $G = \ker(\phi+\Id)^2$ sont de dimension $2$ en explicitant une base de chacun de ces espaces. On notera $(v_1,v_2)$ la base choisie pour l'espace $F$, et $(w_1,w_2)$ la base choisie pour l'espace $G$. Comment justifier que $(v_1,v_2,w_1,w_2)$ forme une base de $\R^4$?

    \item Prouver que l'on a $\phi(F)\subset F$ et $\phi(G)\subset G$. Puis donner l'expression de la matrice de $\phi$ dans la base $(v_1,v_2,w_1,w_2)$.

    \item Soit $\sigma: E\rightarrow E$ l'application linéaire dont la matrice dans la base $(v_1,v_2,w_1,w_2)$ s'écrit:
      $$
        S =
          \begin{pmatrix*}[r]
            1 & 0 &  0 &  0 \\
            0 & 1 &  0 &  0 \\
            0 & 0 & -1 &  0 \\
            0 & 0 &  0 & -1
          \end{pmatrix*}.
      $$
    Prouver que $\sigma$ et $\rho = \phi-\sigma$ sont des endomorphismes qui commutent, et que $\rho$ est nilpotente (on a $\rho^N = 0$ pour $N\gg 0$).

    \item Donner l'expression de la matrice de $\sigma^k$ dans la base $(v_1,v_2,w_1,w_2)$, pour tout $k\in\N$. Donner l'expression de la matrice de $\phi^k = (\sigma + \rho)^k$ dans la base $(v_1,v_2,w_1,w_2)$ en utilisant la formule du binôme.
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo}

  On considère l'application linéaire $\phi: X\mapsto A X$ définie par la matrice:
    $$
      A =
        \begin{pmatrix*}[r]
           1 &  1 &  2 \\
           1 &  1 & -2 \\
          -1 & -3 &  0
        \end{pmatrix*}.
    $$
  \begin{enumerate}
    \item Déterminer le polynôme caractéristique et les valeurs propres de $\phi$. Déterminer une base des espaces caractéristiques de $\phi$ puis expliciter la matrice des composantes de la décomposition de Dunford de $\phi$ dans la base de $\R^4$ obtenue en assemblant les bases des espaces caractéristiques.

    \item Déterminer la matrice de $\phi^n$, pour tout $n\in\N$, dans la base considérée dans la question précédente.
  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo}

  Déterminer le polynôme minimal de l'application linéaire $\psi: X\mapsto B X$ associée à la matrice:
    $$
      B =
        \begin{pmatrix*}[r]
           2 & 3 &  0 & 3 \\
          -1 & 0 &  1 & 2 \\
           0 & 3 &  2 & 3 \\
           1 & 2 & -1 & 0
        \end{pmatrix*}.
    $$
\end{exo}


% ==================================================
\section{Endomorphismes nilpotents}
% ==================================================


%-----------------------------------
\begin{exo}

  Soit $\phi: E\rightarrow E$ un endomorphisme nilpotent. On suppose que l'indice de nilpotence de $\phi$ est égal à $n = \dim(E)$. On fixe $u_1\in E$ tel que $\phi^{n-1}(u_1)\not=0_E$, et on pose $u_i = \phi^{i-1}(u_1)$ pour $i = 2,3,\ldots,n$.

  \begin{enumerate}
    \item Montrer que la famille de vecteurs ainsi obtenue $(u_1,u_2,\ldots,u_n)$ forme une base de $E$ et donner l'expression de la matrice de $\phi$ dans cette base.

    \item On considère la matrice
      $$
        A =
          \begin{pmatrix*}[r]
             2 & 1 & 0 \\
            -2 & 2 & 2 \\
             0 & 1 & 2
          \end{pmatrix*}
      $$
      et l'application linéaire $\phi_A(X) = A X$ qui lui est associée.\\
      Prouver que $N = A - 2 I$ est une matrice nilpotente d'indice de nilpotence $3$. Expliciter la base obtenue en applicant la construction de la question 1, pour un choix de vecteur $u_1\in\R^3$ tel que $(A - 2 I)^2 u_1\not=0_{\R^3}$ laissé au soin de l'étudiant, puis expliciter la matrice de $\phi_A$ dans cette base.
    \end{enumerate}
\end{exo}

\end{document}


