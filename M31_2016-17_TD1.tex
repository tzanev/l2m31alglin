\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{macros_M31}

\begin{document}

\hautdepage{TD1}


% ==================================================
\section{Généralité sur les espaces vectoriels\\ et les applications linéaires}
% ==================================================

%-----------------------------------
\begin{exo}

  Dans la liste suivante, quelles familles de vecteurs $(v_1,\ldots,v_n)\in\Q^3$
  sont libres, génératrices, ou forment des bases de $\Q^3$?

  \begin{multicols}{3}
    \begin{enumerate}
      \item
        $
          \left\{
            \begin{psmallmatrix}1\\[2pt] 0\\[2pt] 1\end{psmallmatrix},
            \begin{psmallmatrix}2\\[2pt] 0\\[2pt] 3\end{psmallmatrix}
          \right\}
        $
      \item
        $
          \left\{
            \begin{psmallmatrix} 1\\[2pt] 0\\[2pt] 1\end{psmallmatrix},
            \begin{psmallmatrix}\,\llap{-}1\\[2pt] 1\\[2pt] 2\end{psmallmatrix},
            \begin{psmallmatrix}\,\llap{-}2\\[2pt] 1\\[2pt] 2\end{psmallmatrix}
          \right\}
        $
      \item
        $
          \left\{
            \begin{psmallmatrix}1\\[2pt] 0\\[2pt] 1\end{psmallmatrix},
            \begin{psmallmatrix}2\\[2pt] 0\\[2pt] 3\end{psmallmatrix},
            \begin{psmallmatrix}\,\llap{-}1\\[2pt] 1\\[2pt] 1\end{psmallmatrix},
            \begin{psmallmatrix}0\\[2pt] 0\\[2pt] 1\end{psmallmatrix}
          \right\}
        $
    \end{enumerate}
  \end{multicols}

  % On rappelle que des vecteurs $(u_1,\dots,u_m)$ forment une famille libre (respectivement génératrice, respectivement une base) d'un espace vectoriel $E$ si, pour tout vecteur $v\in E$ donné, l'équation $x_1 u_1 + \cdots + x_m u_m = v$ possède au plus (respectivement au moins, respectivement une et une seule) solution $(x_1,\dots,x_m)$.

\end{exo}

%-----------------------------------
\begin{exo}

  Soit $\P_n$ l'espace vectoriel des polynômes $P(X)$ à coefficients dans un corps $\K$
  et de degré $\deg P\leq n$.

  \begin{questions}
    \item Soient $\phi: \P_n\rightarrow\P_n$ et $\psi: \P_n\rightarrow\P_n$ les applications telles que $\phi(P(X)) = P(X+1)$ et $\psi(P(X)) = P(X-1)$. Vérifier que ces applications sont linéaires et expliciter leur matrice dans la base naturelle de $\P_n$ constituée des polynômes $(1,X,\ldots,X^n)$. La matrice de $\phi$ sera notée $A$. La matrice de $\psi$ sera notée $B$.

    \item Comment s'écrit l'image d'un polynôme $P(X)$ par l'application composée $\psi\circ\phi$? Quelle relation peut-on déduire de ce résultat quant aux matrices $A$ et $B$?
  \end{questions}

\end{exo}

%-----------------------------------
\begin{exo}

  On travaille dans le $\R$-espace vectoriel $\F$ des fonctions $f: \R\rightarrow\R$.

  \begin{questions}
    \item Prouver que les fonctions $f,g: \R\rightarrow\R$ telles que $f(x) = \cos(x)$ et $g(x) = \sin(x)$ forment une famille libre.

    \begin{indication}
      Soient $\lambda,\mu\in\R$ des scalaires tels que $\lambda f + \mu g = 0$. On évaluera la fonction $(\lambda f + \mu g)(x)$ en quelques valeurs $x \in \R$ bien choisies.
    \end{indication}

    \item On note $\P$ le sous-espace de $\F$ engendré par les fonctions $f$ et $g$. Soit $\phi: \R\rightarrow\R$ une fonction de $\P$. Soit $\phi_\alpha: \R\rightarrow\R$ la fonction telle que $\phi_\alpha(x) = \phi(x+\alpha)$, pour $x\in\R$. Montrer que $\phi_\alpha$ appartient à $\P$ puis vérifier que l'application $T_\alpha: \phi\mapsto\phi_\alpha$ définit une application linéaire $T_\alpha: \P\rightarrow\P$. Comment s'écrit la matrice de $T_\alpha$ dans la base $f,g$?

    \item Quelle est l'image d'une application $\phi$ par l'application composée $T_{\alpha}\circ T_{\beta}$? Que donne le produit des matrices associées à $T_{\alpha}$ et $T_{\beta}$?
  \end{questions}

\end{exo}


% ==================================================
\section{Matrices de changement de base et applications}
% ==================================================

%-----------------------------------
\begin{exo}

  On travaille dans un espace $V$ de dimension $3$ dont une base est notée $(e_1,e_2,e_3)$.

  \begin{questions}
    \item Prouver que les vecteurs $(f_1,f_2,f_3)$ tels que $$f_1 = e_1-e_2+e_3,\quad f_2 = e_1+e_2,\quad f_3 = e_1+e_3$$ forment une base $V$.

    \item Déterminer la matrice $P = P_{(f_1,f_2,f_3)}(e_1,e_2,e_3)$ des coordonnées de $(e_1,e_2,e_3)$ dans la base $(f_1,f_2,f_3)$.

    \item Quelles sont les coordonnées du vecteur $v = e_1+e_2+e_3$ dans la base $(f_1,f_2,f_3)$?
  \end{questions}

\end{exo}

%-----------------------------------
\begin{exo}

  On travaille dans l'espace $V = \R^2$. On note $(e_1,e_2)$ les vecteurs de la base canonique de $\R^2$.

  \begin{questions}
    \item Prouver que les vecteurs
    $$
      u_1 = \begin{psmallmatrix} 1 \\[2pt] 1 \\[2pt] \end{psmallmatrix}
      \quad\text{et}\quad
      u_2 = \begin{psmallmatrix} 1 \\[2pt] \,\llap{-}2 \\[2pt] \end{psmallmatrix}
    $$
    forment une base de $\R^2$.\\
    Comment s'écrit la matrice $P = P_{(e_1,e_2)}(u_1,u_2)$ de changement de base de $(u_1,u_2)$ à $(e_1,e_2)$ et la matrice de changement de base inverse $Q = P_{(u_1,u_2)}(e_1,e_2)$?

    \item On note $L_1$ et $L_2$ les droites de $\R^2$ respectivement engendrées par les vecteurs $u_1$ et $u_2$. Soit $\pi: \R^2\rightarrow\R^2$ la projection sur $L_1$ parallèlement à $L_2$. Comment s'écrit la matrice de $\pi$ dans la base $(u_1,u_2)$? Comment s'écrit la matrice de $\pi$ dans la base $(e_1,e_2)$? (On utilisera le résultat précédent et la formule de changement de bases.)

    \item Soit $\sigma: \R^2\rightarrow\R^2$ l'application linéaire dont la matrice dans la base canonique s'écrit
    $$
      S =
        \begin{psmallmatrix}
          \sfrac{1}{3} & \ \phantom{-}\sfrac{2}{3} \\[2pt]
          \sfrac{4}{3} & \ -\sfrac{1}{3}
        \end{psmallmatrix}.
    $$
    Déterminer $\sigma(u_1)$ et $\sigma(u_2)$. Comment s'écrit la matrice de $\sigma$ dans la base $(u_1,u_2)$? Quelle transformation géométrique représente l'application $\sigma$?
  \end{questions}

\end{exo}


% ==================================================
\section{Méthode du pivot et applications}
% ==================================================

%-----------------------------------
\begin{exo} (exercice guidé d'application du cours)

  On travaille dans l'espace $\R^4$. On note $\underline{e} = (e_1,e_2,e_3,e_4)$ la base naturelle de cet espace. Soit $\phi: \R^4\rightarrow\R^4$ l'application linéaire dont la matrice dans la base $(e_1,e_2,e_3,e_4)$ (au départ et à l'arrivée) s'écrit:
  $$
    A =
      \begin{psmallmatrix}
        1 & 0 & 1 & 2 \\[3pt]
        2 & 1 & 0 & 1 \\[3pt]
        1 & 2 & 1 & 0 \\[3pt]
        0 & 1 & 2 & 1
      \end{psmallmatrix}.
  $$

  \begin{questions}
    \item Utiliser des opérations selon les lignes pour écrire la matrice de $\phi$ sous une forme échelonnée (selon les lignes), interpréter cette construction comme un changement de base à l'arrivée $\underline{e}\mapsto\underline{f}$ dans l'expression de $\phi$, et expliciter la matrice $P = P_{\underline{f}}(e_1,e_2,e_3,e_4)$ correspondant à ce changement de base.

    \item Utiliser le résultat obtenu dans la question précédente pour expliciter une base de $\Im\phi$. Construire également une base de $\Ker\phi$ en utilisant la forme de $\phi$ obtenue dans la question précédente.

    \item Comment s'exprime un vecteur
    $$
      v =
        \begin{psmallmatrix}
          y_1 \\[3pt]
          y_2 \\[3pt]
          y_3 \\[3pt]
          y_4
        \end{psmallmatrix}
    $$
    dans la base $\underline{f} = (f_1,f_2,f_3,f_4)$ considérée dans la question 1?\\
    \begin{indication}
      On peut appliquer la formule de changement de base matricielle ou reporter les opérations sur les lignes considérée dans la construction de la question 1 sur le vecteur $v$ pour obtenir le résultat demandé.
    \end{indication}\\
    Utiliser ce calcul pour expliciter une description de $\Im\phi$ par des équations.
  \end{questions}

\end{exo}

%-----------------------------------
\begin{exo} (exercice guidé d'application du cours)

  On travaille à nouveau dans l'espace $\R^4$. On note toujours $\underline{e} = (e_1,e_2,e_3,e_4)$ la base naturelle de cet espace. On considère l'application linéaire $\phi: \R^4\rightarrow\R^4$ dont la matrice dans la base $(e_1,e_2,e_3,e_4)$ (au départ et à l'arrivée) s'écrit:
  $$
    A =
      \begin{psmallmatrix}
        1 & 0 & 1 & 2 \\[3pt]
        2 & 1 & 0 & 1 \\[3pt]
        1 & 2 & 1 & 0 \\[3pt]
        0 & 1 & 2 & 1
      \end{psmallmatrix},
  $$
  comme dans l'exercice précédent.

  \begin{questions}
    \item Utiliser des opérations selon les colonnes pour écrire la matrice de $\phi$ sous une forme échelonnée (selon les colonnes), interpréter cette construction comme un changement de base au départ de base $\underline{e}\mapsto\underline{e}'$ dans l'expression de $\phi$, et expliciter la matrice $P = P_{\underline{e}}(e_1',e_2',e_3',e_4')$ correspondant à ce changement de base.

    \item Utiliser le résultat obtenu dans la question précédente pour expliciter une base de $\Im\phi$ et une base de $\Ker\phi$.
  \end{questions}

\end{exo}

%-----------------------------------
\begin{exo}

Soit $\phi: \C^4\rightarrow\C^2$ l'application linéaire définie par la matrice
$$
  A =
    \begin{psmallmatrix}
     \phantom{-}1 & -1 & \ i &           -i \\
     -1           & -i & \ 1 & \phantom{-}i
    \end{psmallmatrix}.
$$
Expliciter une base de $\Ker\phi$. L'application $\phi$ est-elle surjective?

\end{exo}

%-----------------------------------
\begin{exo}

  Soit $E$ un espace vectoriel, et $u$ une application linéaire de $E$ dans $E$.
  Dire si les propriétés suivantes sont vraies ou fausses :
  \begin{enumerate}
    \item Si $e_1,e_2,\ldots,e_p$ est libre, il en est de même de $u(e_1),u(e_2),\ldots,u(e_p)$.
    \item Si $u(e_1),u(e_2),\ldots,u(e_p)$ est libre, il en est de même de $e_1,e_2,\ldots,e_p$ .
    \item Si $e_1,e_2,\ldots,e_p$ est génératrice de $E$, il en est de même de $u(e_1),u(e_2),\ldots,u(e_p)$.
    \item Si $u(e_1),u(e_2),\ldots,u(e_p)$ est génératrice de $E$, il en est de même de $e_1,e_2,\ldots,e_p$.
    \item Si $u(e_1),u(e_2),\ldots,u(e_p)$ est une base de $\Im u$, alors $e_1,e_2,\ldots,e_p$ est une base d'un sous-espace vectoriel supplémentaire de $\Ker u$.
  \end{enumerate}

\end{exo}

%-----------------------------------
\begin{exo}

    Soit $f \in \mathcal{L}(E)$ telle que $f^{3} = f^{2} + f + \Id$.

    Montrer que $f$ est un automorphisme de $E$.
\end{exo}

\end{document}


