\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{macros_M31}

\begin{document}

\hautdepage{TD3}


% ==================================================
\section{Espaces supplémentaires}
% ==================================================

%-----------------------------------
\begin{exo}

  On travaille dans l'espace $\C^3$.

  \begin{questions}
    \item
    Construire une base de l'hyperplan
    $$
      H =
        \Bigl\{
          \begin{psmallmatrix}
            x_1 \\[1pt]
            x_2 \\[1pt]
            x_3
          \end{psmallmatrix}
          \in\C^3
          \;\Big|\;
          x_1 = i x_2 - i x_3
        \Bigr\}.
    $$
    On notera $(u_1,u_2)$ les vecteurs de cette base.

    \item Soit $D$ la droite de $\C^3$ engendrée par le vecteur
      $$
        u_3 =
          \begin{psmallmatrix}
            1\\[3pt]
            0\\[3pt]
            1
          \end{psmallmatrix}.
      $$
      Prouver que $H\oplus D = \C^3$.

    \item Déterminer la matrice de la projection sur $H$ parallèlement à $D$ dans la base $(u_1,u_2,u_3)$, puis dans la base naturelle $(e_1,e_2,e_3)$ de $\C^3$ en utilisant une formule de changement de base appropriée que l'on explicitera.

    %\item
    %Déterminer la matrice de la symétrie par rapport à l'hyperplan $H$
    %parallèlement à la droite $D$
    %en utilisant le même plan que pour le calcul de la matrice de la projection
    %dans la question précédente.
  \end{questions}



\end{exo}

%-----------------------------------
\begin{exo}

  On travaille dans l'espace $\F$ des fonctions $f: \R\rightarrow\R$.
  On veut prouver que l'espace des fonctions paires
  $\mathcal{S} = \{f\in\F \;|\; f(x) = f(-x),\,\forall x\in\R\}$
  et l'espace des fonctions impaires
  $\mathcal{A} = \{f\in F \;|\; f(x) = - f(-x),\,\forall x\in\R\}$
  sont supplémentaires dans $\F$.

  \begin{questions}
    \item Prouver $\mathcal{A}\cap\mathcal{S} = \{0_{\F}\}$.

    \item Montrer que l'écriture
      $$
        f(x) = \frac{f(x) + f(-x)}{2} + \frac{f(x) - f(-x)}{2}
      $$
      permet de décomposer chaque $f\in\F$ en somme d'une fonction $f_+\in\mathcal{S}$ et d'une fonction  $f_-\in\mathcal{A}$.

    \item Conclure.
  \end{questions}

\end{exo}

%-----------------------------------
\begin{exo}\hard

  On dit qu'un endomorphisme $\pi: E\rightarrow E$ d'un espace vectoriel $E$ est un projecteur s'il vérifie $\pi^2 = \pi$, où on considère l'endomorphisme composé $\pi^2 = \pi\circ\pi$.

  \begin{questions}
    \item Observer que l'on a $\pi^2 = \pi\Leftrightarrow\pi\circ(\Id-\pi) = (\Id-\pi)\circ\pi = 0$. On note $(*)$ ces relations.

    \item Prouver que l'on a $\Ker(\pi)\oplus\Ker(\Id-\pi) = E$.\\
      \begin{indication}
        On partira de la relation évidente $v = (v-\pi(v)) + \pi(v)$ et on utilisera les relations (*) pour exprimer tout vecteur $v\in E$ comme somme d'un vecteur de $\Ker(\pi)$ et d'un vecteur de $\Ker(\Id-\pi)$, on vérifiera aisément que l'on a en outre $\Ker(\pi)\cap\Ker(\Id-\pi) = \{0_E\}$.
      \end{indication}

    \item Montrer que notre projecteur $\pi$ s'identifie à la projection sur le sous espace $F = \Ker(\Id-\pi)$ parallèlement au sous espace $K = \Ker(\pi)$.
  \end{questions}

\end{exo}

%-----------------------------------
\begin{exo}\hard

  On dit qu'un endomorphisme $\phi: E\rightarrow E$ d'un espace vectoriel $E$ est idempotent s'il vérifie $\phi^2 = \Id$. On suppose que l'on travaille sur un corps de scalaires $\K$ de caractéristique différente de $2$ ({\it i.e.} tel que $2\not=0$).
  \begin{questions}
    \item Observer que l'on a $\phi^2 = \Id\Leftrightarrow(\Id-\phi)\circ(\Id+\phi) = (\Id+\phi)\circ(\Id-\phi) = 0$, puis adapter les méthodes utilisées dans l'exercice précédent pour montrer que l'on a $\Ker(\Id-\phi)\oplus\Ker(\Id+\phi) = E$.
    \item Observer ensuite que notre idempotent $\phi$ s'identifie à la symétrie par rapport au sous espace $F = \Ker(\Id-\phi)$ parallèlement au sous espace $G = \Ker(\Id+\phi)$.
  \end{questions}

\end{exo}

% ==================================================
\section{Diagonalisation des endomorphismes}
% ==================================================


%-----------------------------------
\begin{exo}

  Les matrices suivantes
  $$
    A =
      \begin{psmallmatrix*}[r]
        \,2 & -2 & \ 0 \\[3pt]
          1 &  2 &   1 \\[3pt]
          0 &  2 &   2
      \end{psmallmatrix*},
      \qquad
    B =
      \begin{psmallmatrix*}[r]
        1 &  1 & -2 \\[3pt]
        1 &  0 &  1 \\[3pt]
        1 & -2 &  3
      \end{psmallmatrix*}
      \qquad\text{et}\qquad
    C =
      \begin{psmallmatrix*}[r]
         3 & -1 & \ 1 \\[3pt]
        -1 &  3 &   1 \\[3pt]
         2 &  2 &   2
      \end{psmallmatrix*}.
  $$
  sont-elles diagonalisables (sur $\Q$, $\R$, ou $\C$)?

\end{exo}

%-----------------------------------
\begin{exo}

  Soit $A$ une matrice $n\times n$ à coefficients réels. Soit $\lambda$ une valeur propre complexe de $A$. Montrer que $\bar{\lambda}$ est également valeur propre de $A$ et comparer les dimensions des espaces propres $\Ker(A - \lambda I)$ et $\Ker(A - \bar{\lambda} I)$.

\end{exo}

%-----------------------------------
\begin{exo}

  On considère la matrice
  $$
    A =
      \begin{psmallmatrix*}[r]
        -1 &  2 &  2 \\[3pt]
         2 & -1 & -2 \\[3pt]
        -2 &  2 &  3
      \end{psmallmatrix*}
  $$
  et l'application associée $\phi_A: \R^3\rightarrow\R^3$, définie par $\phi_A(X) = A X$.

  \begin{questions}

    \item Quelles sont les valeurs propres et les dimensions des espaces propres de $\phi_A$?

    \item Constater que $\phi_A$ est diagonalisable. Expliciter une base de $\R^3$ formée de vecteurs propres de $\phi_A$ et écrire la matrice de $\phi_A$ dans cette base.

    \item Comment s'interprète géométriquement l'application $\phi_A: \R^3\rightarrow\R^3$ définie par la matrice $A$?
  \end{questions}

\end{exo}

%-----------------------------------
\begin{exo}

  \begin{questions}
    \item On travaille dans l'espace $\R_{3}[x]$ constitué des polynômes $P(x)$ tels que $\deg(P(x))\leq 3$. Vérifier que l'application $\phi: P(x)\mapsto\phi\big(P(x)\big)$ telle que $$\phi\big(P(x)\big) = (1 - x^2) P'(x) + (1 + 3 x) P(x).$$ définit une application linéaire $\phi: \R_{3}[x]\rightarrow\R_{3}[x]$, puis expliciter la matrice $A$ de cette application $\phi$ dans la base naturelle $(1,x,x^2,x^3)$ de $\R_{3}[x]$.

    \item Déterminer le polynôme caractéristique de $A$. En déduire les valeurs propres et les vecteurs propres de $\phi$.

    \item\hard\hard Retrouver le résultat précédent en résolvant les équations différentielles
    $$
      \frac{y'}{y} = \frac{\lambda - (1 + 3 x)}{1 - x^2},\qquad x\in]-1,1[.
    $$
  \end{questions}

\end{exo}

%-----------------------------------
\begin{exo}

  On considère la matrice $n\times n$
  $$
    B =
    \scalebox{.77}{$
      \begin{pmatrix}
          0    & \cdots &   0    &   1    \\
        \vdots &        & \vdots & \vdots \\
          0    & \cdots &   0    &   1    \\
          1    & \cdots &   1    &   2
      \end{pmatrix}
    $}
  $$
  et l'application associée $\phi_B: \R^n\rightarrow\R^n$, définie par $\phi_B(X) = B X$.

  \begin{questions}
    \item Quelle est la dimension du noyau de $\phi_B$?

    \item Quelles sont les valeurs propres non nulles $\lambda\not=0$ de $\phi_B$?\\
      \begin{indication}
        On pourra résoudre directement l'équation $B X = \lambda X\,\Leftrightarrow\,(B - \lambda I) X = 0$, pour un paramètre $\lambda\not=0$, et donner la dimension de $\Ker(\phi_B - \lambda\Id)$ selon la valeur de $\lambda\not=0$. On peut aussi calculer directement le polynôme caractéristique de $B$, mais les calculs sont un peu plus compliqués.
      \end{indication}

    \item L'application linéaire $\phi_B$ est-elle diagonalisable?
  \end{questions}

\end{exo}

%-----------------------------------
\begin{exo}\hard

Soit $P(t) = t^n - a_{n-1} t^{n-1} - \cdots - a_0$ un polynôme à coefficients complexes. On suppose que les racines de $P(t)$, notées $\lambda_1,\ldots,\lambda_n$, sont distinctes deux à deux.

\begin{questions}
  \item Déterminer les vecteurs propres et les valeurs propres de la matrice
  $$
    A :=
      \scalebox{.77}{$
        \begin{pmatrix}
            0    &   1    & \cdots & \cdots  &   0      \\
          \vdots &   0    &   1    &         & \vdots   \\
          \vdots &        & \ddots & \ddots  & \vdots   \\
            0    & \cdots & \cdots &    0    &   1      \\
           a_0   &  a_1   & \cdots & a_{n-2} &  a_{n-1}
        \end{pmatrix},
      $}
  $$
  puis montrer que $A$ est diagonalisable.\\
  \begin{indication}
    On peut résoudre l'équation $A X = \lambda X$ directement, sans calculer le polynôme caractéristique de $A$.
  \end{indication}

  \item Déduire de la question précédente l'expression du polynôme caractéristique de $A$. Retrouver le résultat obtenu en utilisant un développement selon la dernière ligne de $A$.
\end{questions}

\end{exo}

\end{document}


