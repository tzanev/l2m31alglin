\documentclass[a4paper,11pt,reqno]{amsart}
\usepackage{macros_M31}

\begin{document}

\hautdepage{TD2}


% ==================================================
\section{Permutations}
% ==================================================

%-----------------------------------
\begin{exo}

Calculer la décomposition en cycles et la signature des permutations:
\begin{multicols}{3}
    \begin{enumerate}
      \item
        $
          \rho =
            \begin{psmallmatrix}
              1 & 2 & 3 & 4 & 5 & 6 \\[3pt] 4 & 6 & 1 & 2 & 5 & 3
            \end{psmallmatrix}
        $
      \item
        $
          \tau =
            \begin{psmallmatrix}
              1 & 2 & 3 & 4 & 5 & 6 \\[3pt] 5 & 6 & 1 & 2 & 3 & 4
            \end{psmallmatrix}
        $
      \item
        $
          \sigma =
            \begin{psmallmatrix}
              1 & 2 & 3 & 4 & 5 & 6 \\[3pt] 1 & 4 & 6 & 2 & 3 & 5
            \end{psmallmatrix}
        $
    \end{enumerate}
  \end{multicols}

\end{exo}

%-----------------------------------
\begin{exo}
  On note $\theta\in\Sigma_{2 n}$ la permutation:
  $$
    \theta =
    \begin{psmallmatrix}
      1   & \cdots & n   & n+1 & \cdots & n+n \\[3pt]
      n+1 & \cdots & n+n & 1   & \cdots & n
    \end{psmallmatrix}.
  $$
  \begin{questions}
    \item Comment $\theta$ se décompose-t-elle en produit de cycles?
    \item Quel est le nombre d'inversions et la signature de $\theta$?
  \end{questions}
\end{exo}

%-----------------------------------
\begin{exo}

  Peut-on construire une permutation qui a l'ensemble des paires $i<j$ tout entier comme ensemble d'inversions, c'est à dire telle que l'on ait $s(j)>s(i)$ pour toute paire $i<j$?
\end{exo}

%-----------------------------------
\begin{exo}

  Soit $c = (a_1\ a_2\ \cdots\ a_r)$ un cycle de longueur $r$ dans le groupe des permutations de $\Sigma_n$. Soit $s\in\Sigma_n$ une permutation quelconque. Comment s'écrit la décomposition en cycles de la permutation composée $\theta = s c s^{-1}$?

  \begin{indication}
    Que vaut $\theta(s(a_i))$, pour $i = 1,\dots,r$? Que vaut $\theta(s(b))$, lorsque $b\not=a_1,\dots,a_r$?
  \end{indication}

\end{exo}

% ==================================================
\section{Déterminants}
% ==================================================

%-----------------------------------
\begin{exo} (Quiz)

  \begin{questions}
    \item On a
      $$
        \Det
        \left(
          \begin{psmallmatrix}a_{1 1} & a_{1 2} \\[3pt] a_{2 1} & a_{2 2}\end{psmallmatrix}
          +
          \begin{psmallmatrix}b_{1 1} & b_{1 2} \\[3pt] b_{2 1} & b_{2 2}\end{psmallmatrix}
        \right)
        = \Det\begin{psmallmatrix}a_{1 1} & a_{1 2} \\[3pt] a_{2 1} & a_{2 2}\end{psmallmatrix}+
          \Det\begin{psmallmatrix}b_{1 1} & b_{1 2} \\[3pt] b_{2 1} & b_{2 2}\end{psmallmatrix}.
      $$

      \textit{Vrai} ou \textit{Faux}?

    \item On a
      $$
        \Det
        \left(
          \lambda\begin{psmallmatrix}a_{1 1} & a_{1 2} \\[3pt] a_{2 1} & a_{2 2}\end{psmallmatrix}
        \right) =
        \lambda\Det
          \begin{psmallmatrix}a_{1 1} & a_{1 2} \\[3pt] a_{2 1} & a_{2 2}\end{psmallmatrix}.
      $$

      {\it Vrai} ou {\it Faux}?
      Si «Faux», quelle relation lie $\Det(\lambda A)$ et $\Det(A)$?

    \item Soient $A$ et $B$ des matrices $n\times n$. On a $\Det(A B) = \Det(B A)$ même si $A B\not=B A$.

      {\it Vrai} ou {\it Faux}?

    \item Soient $A$ et $B$ des matrices $n\times n$.
      On dit que $A$ est semblable à $B$ si on a $A = P B P^{-1}$ pour une matrice inversible $P$.
      Si $A$ est semblable à $B$, alors on a $\Det(A) = \Det(B)$.

      {\it Vrai} ou {\it Faux}?

  \end{questions}
\end{exo}

%-----------------------------------
\begin{exo} (Quiz)
  \begin{questions}
    \item Que peut-on dire du déterminant d'une matrice $n\times n$ nilpotente (c'est à dire vérifiant $A^N = 0$ pour un certain $N>0$)?

    \item Que peut-on dire du déterminant d'une matrice $n\times n$ telle que $A^2 = I$?

    \item Que peut-on dire du déterminant d'une matrice $n\times n$ antisymétrique (c'est à dire telle que ${}^t A = -A$) lorsque $n$ est impair?

  \end{questions}
\end{exo}


%-----------------------------------
\begin{exo}
  On note $j = e^{\frac{2i\pi}{3}}$. On utilisera que $j^3 = 1$ et $1+j+j^2 = 0$.
  \begin{enumerate}
    \item Montrer que les vecteurs
      $$
        u_1 = \begin{psmallmatrix} 1 \\[3pt] 1 \\[3pt] 1 \end{psmallmatrix},\quad
        u_2 = \begin{psmallmatrix*}[l] \,1 \\[3pt] j \\[3pt] j^2 \end{psmallmatrix*},\quad
        u_3 = \begin{psmallmatrix*}[l] \,1 \\[3pt] j^2 \\[3pt] j \end{psmallmatrix*}
      $$
      constituent une base de $\C^3$.

    \item\label{q2} Soit $\phi: \C^3\rightarrow\C^3$ l'application linéaire associée à la matrice:
      $$ \arraycolsep=3pt\def\arraystretch{.7}
        A =
          \begin{pmatrix}
            a & b & c \\
            b & c & a \\
            c & a & b
          \end{pmatrix}
      $$
      Calculer $\phi(u_1),\phi(u_2),\phi(u_3)$ et donner l'expression de la matrice de $\phi$ dans la base $(u_1,u_2,u_3)$ au départ et à l'arrivée.

    \item Donner une factorisation de $3 a b c - a^3 - b^3 - c^3$ en produit d'expressions linéaires en $a,b,c$.
    \begin{indication}
      On calculera $\Det(\phi)$ en utilisant la matrice obtenue dans la question \ref{q2}).
    \end{indication}

  \end{enumerate}
\end{exo}

%-----------------------------------
\begin{exo}
  \begin{questions}
    \item Calculer le déterminant de la matrice
      $$
        V(\alpha,\beta,\gamma) =
        \begin{psmallmatrix}
          1        & 1       & 1      \\[4pt]
          \alpha   & \beta   & \gamma \\[4pt]
          \alpha^2 & \beta^2 & \gamma^2
        \end{psmallmatrix}
      $$
      sous forme factorisée. Utiliser le résultat obtenu pour donner des conditions sur les paramètres $(\alpha,\beta,\gamma)$ assurant que cette matrice $V(\alpha,\beta,\gamma)$
      est inversible.

    \item Calculer le déterminant de la matrice
      $$
        A(t) =
          \begin{psmallmatrix}
            1 & 1 & t \\[4pt]
            1 & t & 1 \\[4pt]
            t & 1 & 1
          \end{psmallmatrix}
      $$
      sous forme factorisée. Utiliser ce résultat pour déterminer pour quelles valeurs du paramètre $t$ cette matrice est inversible.

    \item Donner une expression factorisée du déterminant de la matrice
      $$
        P =
          \begin{psmallmatrix*}[r]
            a & 1  & -1 & 1  \\[4pt]
            1 & a  & 1  & -1 \\[4pt]
           -1 & 1  & a  & 1  \\[4pt]
            1 & -1 & 1  & a
          \end{psmallmatrix*}
      $$
      et préciser les valeurs du paramètre $a\in\Q$ pour lesquelles cette matrice $P$ est inversible.


  \end{questions}
\end{exo}

%-----------------------------------
\begin{exo}

  Le but de l'exercice est de déterminer le déterminant de la matrice
  $$\arraycolsep=5pt\def\arraystretch{.7}
    \begin{pmatrix}
        x    &   0    & \cdots &   0    &   b    \\
        0    & \ddots & \ddots & \vdots & \vdots \\
      \vdots & \ddots & \ddots &   0    &   b    \\
        0    & \cdots &   0    &   x    &   b    \\
        a    & \cdots &   a    &   a    &   x
    \end{pmatrix},
  $$
  pour des paramètres $a,b,x$. On note $D_n$ le déterminant de cette matrice en dimension $n$.

  \begin{questions}

    \item Prouver, par un développement approprié (selon une ligne ou une colonne bien choisie), que l'on a la relation $D_n = x D_{n-1} - a b x^{n-2}$, pour tout $n\geq 3$.

    \item Puis établir la formule $D_n = x^n - (n-1) a b x^{n-2}$, pour $n\geq 2$.

  \end{questions}

\end{exo}

%-----------------------------------
\begin{exo}\hard

  Le but de l'exercice est de déterminer le déterminant de la matrice
  $$\arraycolsep=5pt\def\arraystretch{.7}
    A(a,b,c) =
      \begin{pmatrix}
          a    &   b    & \cdots & \cdots &   b    \\
          c    &   a    & \ddots &        & \vdots \\
        \vdots & \ddots & \ddots & \ddots & \vdots \\
        \vdots &        & \ddots &   a    &   b    \\
          c    & \cdots & \cdots &   c    &   a
      \end{pmatrix}
  $$
  pour des paramètres $a,b,c$ tels que $b\not=c$.

  \begin{questions}

    \item Montrer que $P(t) = \Det A(a-t,b-t,c-t)$ est un polynôme de degré $1$ en $t$.

      \begin{indication}
        On combinera des opérations et un développement selon une ligne ou une colonne.
      \end{indication}

    \item Déterminer les valeurs $P(b)$ et $P(c)$. En déduire l'expression du polynôme $P(t)$ en utilisant ce résultat.

    \item\label{q3} Conclure quant à l'expression de $\Det A(a,b,c)$ pour des paramètres tels que $b\not=c$.

    \item Quelle est la valeur de $\Det A(a,b,c)$ lorsque $b = c$?

      \begin{indication}
        On peut calculer ce déterminant directement ou faire $b\rightarrow c$ dans l'expression obtenue dans la question \ref{q3}).
      \end{indication}
  \end{questions}

\end{exo}

%-----------------------------------
\begin{exo}\hard\hard

  On considère le déterminant
  $$\arraycolsep=3pt\def\arraystretch{.7}
    V_n(\alpha_1,\ldots,\alpha_n) =
      \Det
        \begin{pmatrix*}[l]
          &        1         & \cdots &        1         & \cdots &        1         \\
          &     \alpha_1     & \cdots &     \alpha_j     & \cdots &     \alpha_n     \\
          &      \vdots      &        &      \vdots      &        &      \vdots      \\
          &   \alpha_1{}^r   & \cdots &   \alpha_j{}^r   & \cdots &   \alpha_n{}^r   \\
          &      \vdots      &        &      \vdots      &        &      \vdots      \\
          & \alpha_1{}^{n-1} & \cdots & \alpha_j{}^{n-1} & \cdots & \alpha_n{}^{n-1}
        \end{pmatrix*}
  $$
  pour $\alpha_1,\ldots,\alpha_n\in\C$.

  \begin{questions}

    \item Que peut-on dire de ce déterminant $V_n(\alpha_1,\ldots,\alpha_n)$ si on a $\alpha_i = \alpha_j$ pour une paire $i\not=j$?

    \item On fixe des complexes $\alpha_1,\ldots,\alpha_{n-1}$ deux à deux distincts. Prouver que l'application $t\mapsto V_n(\alpha_1,\ldots,\alpha_{n-1},t)$ est un polynôme de degré $n-1$ en $t$.
    \begin{enumerate}
      \item Quelle est la valeur de ce polynôme en $t=0$?
      \item Quelles sont les racines de ce polynôme?

        \begin{indication}
          que peut-on déduire des observations de la question précédente?
        \end{indication}
      \item \textit{(Conclusion)} Comment s'écrit l'application $t\mapsto V_n(\alpha_1,\ldots,\alpha_{n-1},t)$?
    \end{enumerate}

    \item Utiliser le résultat de la question précédente pour trouver par récurrence une formule calculant $V_n(\alpha_1,\ldots,\alpha_n)$.

  \end{questions}

\end{exo}


\end{document}


