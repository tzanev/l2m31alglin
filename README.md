# [<img src="https://about.gitlab.com/images/press/logo/svg/gitlab-logo-500.svg" height="50" style="vertical-align: middle">](https://gitlab.univ-lille.fr/tzanev/l2m31alglin/) [l2m31alglin](https://tzanev.gitlabpages.univ-lille.fr/l2m31alglin/)

Les feuilles de td du module M31 - « Algèbre linéaire » de la L2 Mathématiques à l'Université de Lille.

## 2016/17

Dans [ce dépôt](https://gitlab.univ-lille.fr/tzanev/l2m31alglin/) vous pouvez trouver les sources LaTeX et les PDFs des documents suivants :

### Les feuilles de td
_(compilés avec [tectonic](https://tectonic-typesetting.github.io))_

- TD n°1 **[[pdf](M31_2016-17_TD1.pdf)]** [[tex](M31_2016-17_TD1.tex)]
- TD n°2 **[[pdf](M31_2016-17_TD2.pdf)]** [[tex](M31_2016-17_TD2.tex)]
- TD n°3 **[[pdf](M31_2016-17_TD3.pdf)]** [[tex](M31_2016-17_TD3.tex)]
- TD n°4 **[[pdf](M31_2016-17_TD4.pdf)]** [[tex](M31_2016-17_TD4.tex)]

### Fichiers complètementaires

Pour compiler ces fichiers vous avez besoin de [macros_M31.sty](macros_M31.sty).

### Récupération intégrale

Vous pouvez obtenir [ce dépôt](https://gitlab.univ-lille.fr/tzanev/l2m31alglin/) de deux façons faciles :

- en téléchargeant le [zip](https://gitlab.univ-lille.fr/tzanev/l2m31alglin/-/archive/main/l2m31alglin-main.zip) qui contient la dernière version des fichiers,
- en clonant le dépôt entier, l'historique y compris, en utilisant la commande `git` suivante

  ```shell
  git clone https://gitlab.univ-lille.fr/tzanev/l2m31alglin.git .
  ```


---


